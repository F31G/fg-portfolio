# FG Portfolio

http://fg-portfolio.surge.sh

https://gitlab.com/F31G/fg-portfolio

Projet "Mon plus beau Portfolio", suite à Brief du 21.12.2020 sur Simplon.co


## Consignes :

Mon plus beau portolio



Avec le stage qui approche, c'est le moment de préparer un portfolio à présenter. Ce sera l'occasion pour nous d'évaluer tout ce que vous avez vu depuis le début de la formation.
Livrables

Les maquettes du site réalisé. Un dépôt Gitlab. Un lien vers une version en ligne du portfolio.
Référentiels

Développeur⋅se web et web mobile
Contexte du projet

Nous vous demandons de réaliser et mettre en ligne votre portolio.

Il doit avoir au moins les éléments suivants :

    Présentation de votre parcours
    Présentation de vos compétences et des technologies que vous connaissez
    Présentation de vos projets (avec un lien vers un dépôt gitlab et, si possible, une version en ligne)
    Vos coordonnées ainsi qu'un formulaire de contact (qui fonctionne).
    En bonus, vous pouvez proposer une partie blog.

Votre portfolio doit être attractif et lisible.
IMPORTANT

L’objectif de cette évaluation est de voir si nos propositions fonctionnent pour vous, si nous devons vous accompagner plus sur certains points.

Nous n’allons pas vous noter ou juger la qualité de votre travail.

Il n'y a pas de mauvaise réponse, il n'y a pas de note, juste de la bienveillance pour vous guider dans les meilleures conditions possibles.
Modalités pédagogiques

Travail en individuel.

Le choix des technologies est libre (framework CSS ou CSS Vanilla ou SASS ou autres, JS ou PHP, etc).

Rendu avant le mercredi 23 décembre à 12h00 (sinon, nous ne corrigerons pas).
Critères pour la revue de code

Lors de la revue de code, nous utiliserons les critères suivants :

    Le portfolio est responsive.
    L'ecoindex est A ou B.
    Le Performance Score est au moins de 80%.
    Le site ne comporte aucune erreur d'accessibilité.
    Le code est propre est indenté. Tous les critères de performance des compétences C1, C2 et C3 présentées dans le référentiel.

Critères de performance

    Les maquettes de chaque page sont réalisées (sur papier ou avec un outil numérique)
    Le dépôt gitlab contient un README présentant le projet, son fonctionnement et un lien vers la version en ligne.
    Le dépôt gitlab fait l'objet de commits réguliers, chaque jour.

Attention!! Tous ces critères doivent être remplis pour que nous corrigions votre rendu.
Modalités d'évaluation

Revue de code avec le(s) formateur(s).
